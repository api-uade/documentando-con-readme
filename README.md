# Documentando con README

![Alt](Database.png)

## Diagrama de clases
```mermaid
classDiagram
Class01 <|-- AveryLongClass : Cool
Class03 *-- Class04
Class05 o-- Class06
Class07 .. Class08
Class09 --> C2 : Where am i?
Class09 --* C3
Class09 --|> Class07
Class07 : equals()
Class07 : Object[] elementData
Class01 : size()
Class01 : int chimp
Class01 : int gorilla
Class08 <--> C2: Cool label
```

## Diagram of relationships

```mermaid
	graph TD;
	client-->client_profile;
	client-->customization;
	client-->country;
	client-->language;
	client-->zone;
	client-->section_client;
	layout-->section_client;
	section-->section_client;
	content-->section_client;
	section_client-->section_extension;
	content-->section_extension;
	extension-->section_extension;
```

# Web Services Documentation

##### Table of Contents  
1. [Client](#client)
2. [Client Profile](#client-profile)  
3. [Zone](#zone)
4. [Customization](#customization)
5. [Content](#content)
5. [Layout](#layout)
6. [Section Client](#section-client)


##### Tabla 
| **Name**  | **Description** | **Data Type** |
| ---      | ---       | ---       |
| name  | Name of the client | String  |
| cname | CNAME  | String  |
| url   | URL's to access to the client | String |
| title | Title of the project | String |
| country_id | Id of the country associate to the table country | Integer |
| zone_id | Id of the zone associate to the table zone | Integer |
| language_id | Id of the language associate to the table language | Integer |
| ssl | SSL security if it's available must be in 1 or 0 disable | TinyInt |
| logo | Logo name locate in your images directory | String |
| twitter_account | Name of your twitter account without https://www.twitter.com/ | String |
| facebok_account | Name of your facebook account without https://www.facebook.com/ | String |
| google_analytics | Alphanumeric reference for your Google Analytics | String |
| facebook_app_id | Facebook App Id | String |
| facebook_secret | Facebook Secret | String |
| security_endpoint | Url of your endpoint | String |
| status | If the client it's available set in 1 or 0 for disable | TinyInt |

#
```sh
npx run docs:generate -- --template=winter --description="Sparkling and frozen" --elements="snow,frost,ice" --snowflakes=20
```
```console
$ npm install
```

```console
$ npm start
```
